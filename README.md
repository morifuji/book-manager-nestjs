# 書籍管理アプリケーション

実装済み機能

- ユーザー登録
- ユーザー更新
- 書籍登録
- 書籍検索
- 貸し出し
- 返却

## API仕様書

```sh
yarn start:dev

# in other console
## browse in window
open http://localhost:3000/api
## browse in window by json
open http://localhost:3000/api-json
```
