-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Book" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "title" TEXT NOT NULL,
    "coverImageUrl" TEXT,
    "description" TEXT,
    "publishedAt" DATETIME,
    "currentBorrowingUserId" TEXT,
    CONSTRAINT "Book_currentBorrowingUserId_fkey" FOREIGN KEY ("currentBorrowingUserId") REFERENCES "User" ("id") ON DELETE SET NULL ON UPDATE CASCADE
);
INSERT INTO "new_Book" ("coverImageUrl", "description", "id", "publishedAt", "title") SELECT "coverImageUrl", "description", "id", "publishedAt", "title" FROM "Book";
DROP TABLE "Book";
ALTER TABLE "new_Book" RENAME TO "Book";
CREATE INDEX "Book_title_idx" ON "Book"("title");
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
