/*
  Warnings:

  - You are about to drop the column `authorId` on the `Book` table. All the data in the column will be lost.

*/
-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Book" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "title" TEXT NOT NULL,
    "coverImageUrl" TEXT,
    "description" TEXT,
    "publishedAt" DATETIME
);
INSERT INTO "new_Book" ("coverImageUrl", "description", "id", "publishedAt", "title") SELECT "coverImageUrl", "description", "id", "publishedAt", "title" FROM "Book";
DROP TABLE "Book";
ALTER TABLE "new_Book" RENAME TO "Book";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
