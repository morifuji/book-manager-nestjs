import { HttpException, HttpStatus, Injectable, Post } from '@nestjs/common';
import { BOOK_JOIN_POLICY } from 'src/book/book.service';
import { PrismaService } from 'src/prisma/prisma.service';
import { ReqBorrow } from './borrowing.dto';

const BORROWING_JOIN_POLICY = {
    include: {
        user: true,
        book: BOOK_JOIN_POLICY
    }
}

@Injectable()
export class BorrowingService {

    constructor(private prismaService: PrismaService) {

    }

    async borrow(reqBorrow: ReqBorrow) {
        const now = new Date()

        const targetBorriwng = await this.searchByBookIdList([reqBorrow.bookId])
        if (targetBorriwng.some(b=>b.returnedAt===null)) {
            throw new HttpException({
                error: 'すでに書籍が借りられています',
              }, HttpStatus.BAD_REQUEST);
        }
        
        return this.prismaService.borrowing.create({
            data: {
                startedAt: now,
                scheduledReturnedAt: reqBorrow.scheduledReturnedAt,
                user: {
                    connect: {
                        email: reqBorrow.user.email 
                    }
                },
                book: {
                    connect: {
                        id: reqBorrow.bookId
                    }
                }
            },
            ...BORROWING_JOIN_POLICY
            
        }).catch(err=>{
            if (err.code === "P2025") {
                throw new HttpException({
                    error: '存在しないユーザーまたは書籍が指定されています',
                  }, HttpStatus.NOT_FOUND);
            }
            throw err
        })
    }


    searchByBookIdList(bookIdList: string[]) {
        return this.prismaService.borrowing.findMany({
            where: {
                bookId: {
                    in: bookIdList
                },
                returnedAt: null
            },
            ...BORROWING_JOIN_POLICY
        })
    }

    async return(bookId: string, userEmail: string) {

        const targetBorrowing = await this.prismaService.borrowing.findFirst({
            where: {
                user: {
                    email: userEmail
                },
                bookId,
                returnedAt: null
            },
        })

        if (targetBorrowing===null) {
            throw new HttpException({
                error: '存在しない貸し出し情報が指定されています',
              }, HttpStatus.NOT_FOUND);
        }

        const now = new Date()
        const updatedBorrowing = await this.prismaService.borrowing.update({
            where: {
                userId_bookId_startedAt: {
                    bookId,
                    userId: targetBorrowing.userId,
                    startedAt: targetBorrowing.startedAt
                }
            },
            data: {
                returnedAt: now,
                book: {
                    update: {
                        lastReturnedAt: now
                    }
                }
            },
            ...BORROWING_JOIN_POLICY
        })

        return updatedBorrowing
    }
}
