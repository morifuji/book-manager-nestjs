import { Body, Controller, Get, Param, ParseArrayPipe, Post, Query } from '@nestjs/common';
import { ApiCreatedResponse, ApiTags } from '@nestjs/swagger';
import { ReqBookIdList, ReqBorrow, ResBorrowing } from './borrowing.dto';
import { BorrowingService } from './borrowing.service';

@ApiTags('borrowings')
@Controller('borrowings')
export class BorrowingController {

    constructor(private borrowingService: BorrowingService){}

    @Post()
    @ApiCreatedResponse({
        type: ResBorrowing
    })
    borrow(@Body() reqBorrow: ReqBorrow): Promise<ResBorrowing> {
        return this.borrowingService.borrow(reqBorrow)
    }


    @Post(":bookId/:userEmail")
    @ApiCreatedResponse({
        type: ResBorrowing
    })
    return(@Param("bookId") bookId: string, @Param("userEmail") userEmail: string): Promise<ResBorrowing> {
        return this.borrowingService.return(bookId, userEmail)
    }

    @Get("")
    @ApiCreatedResponse({
        type: ResBorrowing,
        isArray: true
    })
    searchByBookIdList(@Query() bookIdList: ReqBookIdList): Promise<ResBorrowing[]> {
        return this.borrowingService.searchByBookIdList(bookIdList.bookIdList)
    }
}
