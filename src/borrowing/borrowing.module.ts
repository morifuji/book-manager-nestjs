import { Module } from '@nestjs/common';
import { BookModule } from 'src/book/book.module';
import { PrismaModule } from 'src/prisma/prisma.module';
import { BorrowingController } from './borrowing.controller';
import { BorrowingService } from './borrowing.service';

@Module({
  imports: [BookModule, PrismaModule],
  controllers: [BorrowingController],
  providers: [BorrowingService]
})
export class BorrowingModule {}
