import { Borrowing } from '.prisma/client';
import { Body, Controller, Post } from '@nestjs/common';
import { ApiCreatedResponse, ApiProperty } from '@nestjs/swagger';
import { Transform, Type } from 'class-transformer';
import { IsDate, IsDateString, IsEmail, IsNotEmpty, IsObject, IsUUID, ValidateNested } from 'class-validator';
import { ResBook } from 'src/book/book.dto';
import { iso8601Str2Date } from 'src/_transformer/iso8601Str2Date';


class ReqBorrowUser {
    @IsEmail()
    @IsNotEmpty()
    email: string
}


export class ReqBorrow{
    @IsNotEmpty()
    bookId: string

    @IsNotEmpty({ message: "著者情報を入力してください" })
    @ValidateNested() // NOTE: arrayとobjectを区別しないので、IsObjectとIsArrayを必ず併用
    @IsObject({ message: "著者情報は不正なフォーマットです" })
    @Type(() => ReqBorrowUser)
    user: ReqBorrowUser

    @IsDate({ message: '発売日時のフォーマットが正しくありません' })
    @IsNotEmpty()
    @Transform((v)=>iso8601Str2Date(v.value))
    scheduledReturnedAt: Date
}

class ResUser {
    id: string
    email: string
}

export class ResBorrowing {
    @ApiProperty()
    userId: string;
    @ApiProperty()
    bookId: string;
    @ApiProperty()
    startedAt: Date;
    @ApiProperty()
    returnedAt: Date | null;
    @ApiProperty()
    scheduledReturnedAt: Date;
    @ApiProperty()
    book: ResBook
    @ApiProperty()
    user: ResUser
}


export class ReqReturn {
    @IsNotEmpty()
    @IsUUID()   
    bookId: string
}

export class ReqBookIdList {
    @IsUUID("all", {each:true})
    bookIdList: string[];
  }