
type NonNullableKey<T> = {
    [K in keyof T]-?: null extends T[K] ? never : K
}[keyof T]

export type NullableToUndefinable<T> = Pick<T, NonNullableKey<T>> & {
    [K in keyof Omit<T, NonNullableKey<T>>]?: NonNullable<T[K]>
}