import { Test, TestingModule } from '@nestjs/testing';
import { ApplicationServiceTestModule } from 'src/application-service-test/application-service-test.module';
import { UserRepository } from './user.domain';
import { UserDomainService } from './user.domain.service';
import { UserService } from './user.service';

describe('UserService', () => {
  let service: UserService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ApplicationServiceTestModule],
      providers: [UserService, UserDomainService, UserRepository],
    }).compile();

    service = module.get<UserService>(UserService);
  });

  it('should be registered', async () => {
    expect(service).toBeDefined();

    await service.registerUser({
      email: "hrhrwe@hw.hrwhrw",
      name: "gewhigwo23"
    })
  });
});
