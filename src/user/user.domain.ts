import { User } from '.prisma/client';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Expose, plainToClass } from 'class-transformer';
import { PrismaService } from 'src/prisma/prisma.service';

export class DomainUser implements Omit<User, "id"> {
    @Expose() readonly id: string | undefined;

    @Expose() email: string;

    @Expose() name: string | null;

    constructor(email: string, name: string) {
        if (name === "") {
            throw new HttpException({
                error: '名前は１文字以上入力してください',
            }, HttpStatus.BAD_REQUEST);
        }

        this.email = email
        this.name = name || null
    }

    updateName(name: string) {
        if (name === "") {
            throw new HttpException({
                error: '名前は１文字以上入力してください',
            }, HttpStatus.BAD_REQUEST);
        }
        this.name = name
    }
}

@Injectable()
export class UserRepository {

    constructor(private prismaService: PrismaService) { }

    private _reconstruct(user: User) {
        return plainToClass(DomainUser, user, { excludeExtraneousValues: true })
    }

    async save(user: DomainUser) {
        if (user.id === undefined) {
            const data = await this.prismaService.user.create({ data: user })
            return this._reconstruct(data)
        }

        const data = await this.prismaService.user.update({
            data: user,
            where: { id: user.id }
        })
        return this._reconstruct(data)
    }

    async findByEmail(email: string) {
        const data = await this.prismaService.user.findUnique({
            where: {
                email
            }
        })
        if (data === null) return null
        return this._reconstruct(data)
    }
}

// interface Entity{
//     id: string|number|undefined
// }

// class BaseRepository<T extends Entity> {
//     entityName: string = ""

//     constructor(private prismaService: PrismaService) {}

//     async save(data: T) {
//         if (data.id === undefined) {
//             const zzzzz = await this.prismaService[this.entityName].create({data})
//             return DomainUser._construct(zzzzz)
//         }

//         const zzzz = await this.prismaService[this.entityName].update({
//             data: data,
//             where: {id: data.id}
//         })
//         return DomainUser._construct(zzzz)
//     }
// }
