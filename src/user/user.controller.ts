import { Body, Controller, Get, Patch, Post } from '@nestjs/common'
import { ApiResponse, ApiTags } from '@nestjs/swagger'
import { ReqRegisterUser, ResUser } from './user.dto'
import { UserService } from './user.service'

@ApiTags('users')
@Controller('users')
export class UserController {

    constructor(private userService: UserService){}

    @Post()
    @ApiResponse({
        type: ResUser
    })
    async registerUser(@Body() req: ReqRegisterUser): Promise<ResUser>{
        const res = await this.userService.registerUser(req)
        assertIdIsSet(res)
        return res
    }

    @Patch()
    @ApiResponse({
        type: ResUser
    })
    async updateUser(@Body() req: ReqRegisterUser): Promise<ResUser>{
        const res = await this.userService.updateUserByEmail(req)
        assertIdIsSet(res)
        return res
    }
}
