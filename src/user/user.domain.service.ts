import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { ReqRegisterUser } from './user.dto';

@Injectable()
export class UserDomainService {
    constructor(private prismaService: PrismaService) {

    }

    isExistedEmail(email: string) {
        return this.prismaService.user.count({
            where: {
                email
            }
        }).then((count) => count!==0)
    }
}
