import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsNotEmpty, IsOptional, MaxLength } from "class-validator";



export class ReqRegisterUser {
  @IsNotEmpty()
  @IsEmail()
  email: string

  @IsOptional()
  @MaxLength(255, { message: 'nicknameは$constraint1文字以内で入力してください' })
  name?: string
}

export class ResUser {
  @ApiProperty()
  id: string
  @ApiProperty()
  email: string
  @ApiProperty()
  name: string|null
}