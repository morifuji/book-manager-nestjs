import { Module } from '@nestjs/common'
import { PrismaModule } from 'src/prisma/prisma.module';
import { UserController } from './user.controller'
import { UserService } from './user.service';
import { UserDomainService } from './user.domain.service';
import { UserRepository } from './user.domain';

@Module({
  controllers: [UserController],
  providers: [UserService, UserDomainService, UserRepository],
  imports: [PrismaModule]
})
export class UserModule {}
