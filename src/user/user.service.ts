import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { DomainUser, UserRepository } from './user.domain';
import { UserDomainService } from './user.domain.service';
import { ReqRegisterUser } from './user.dto';

@Injectable()
export class UserService {
    constructor(private userDomainService: UserDomainService, private userRepository: UserRepository) { }

    async registerUser(req: ReqRegisterUser) {
        const email = req.email
        if (await this.userDomainService.isExistedEmail(email)) {
            throw new HttpException({
                error: 'すでに登録されているメールアドレスです',
            }, HttpStatus.BAD_REQUEST);
        }

        const user = new DomainUser(email, req.name!!)

        return await this.userRepository.save(user)
    }

    async updateUserByEmail(req: ReqRegisterUser) {
        const user = await this.userRepository.findByEmail(req.email)
        if (user === null) {
            throw new HttpException({
                error: '該当のユーザーが存在しません',
            }, HttpStatus.BAD_REQUEST);
        }

        user.updateName(req.name!!)
        return this.userRepository.save(user)
    }
}
