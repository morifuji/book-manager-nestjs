import {
    Body, Controller, Get, Post, Query,
} from '@nestjs/common'
import { ApiOkResponse, ApiTags } from '@nestjs/swagger'
import { ReqRegisterBook, ReqSearchBook, ReqSearchOrderEnum, ResBook } from './book.dto'
import { BookService } from './book.service'

@ApiTags('books')
@Controller('books')
export class BookController {
    constructor(private bookService: BookService) { }

    @Post()
    @ApiOkResponse({
        type: ResBook,
    })
    async registerBook(@Body() _reqRegisterBook: ReqRegisterBook): Promise<ResBook> {
        const registeredBook = await this.bookService.register(_reqRegisterBook)
        return registeredBook
    }

    @Get()
    @ApiOkResponse({
        type: ResBook,
        isArray: true
    })
    async searchBook(@Query() req: ReqSearchBook): Promise<ResBook[]> {
        return this.bookService.searchBooks(req.order || ReqSearchOrderEnum.LATEST_REGISTERED, req.query)
    }
}
