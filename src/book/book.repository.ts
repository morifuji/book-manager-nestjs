import { Injectable } from '@nestjs/common'
import axios from 'axios'

type GoogleBookApiType = {
  totalItems: number,
  items: {
    volumeInfo: {
      title: string,
      authors?: string[],
      publishedDate: string,
      description: string,
      imageLinks: { thumbnail: string }
    }
  }[] | undefined
}

@Injectable()
export class BookRepository {
  async fetchSimilarBooks(searchQuery: string) {
    const res = await axios.get<GoogleBookApiType>(`https://www.googleapis.com/books/v1/volumes?q=${encodeURIComponent(searchQuery)}`)
    return res.data
  }
}
