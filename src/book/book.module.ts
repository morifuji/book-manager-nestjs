import { Module } from '@nestjs/common'
import { PrismaModule } from 'src/prisma/prisma.module'

import { BookController } from './book.controller'
import { BookRepository } from './book.repository'
import { BookService } from './book.service'

@Module({
  imports: [PrismaModule],
  providers: [BookService, BookRepository],
  controllers: [BookController],
  exports: [BookService]
})
export class BookModule { }
