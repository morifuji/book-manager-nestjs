import { Injectable } from '@nestjs/common'
import { PrismaService } from 'src/prisma/prisma.service'
import { ReqRegisterBook, ReqSearchOrderEnum } from './book.dto'
import { BookRepository } from './book.repository'

export const BOOK_JOIN_POLICY = {
  include: {
    bookAuthors: {
      include: {
        author: true
      }
    }
  }
}

@Injectable()
export class BookService {
  constructor(private prismaService: PrismaService, private bookRepository: BookRepository) { }

  async register(book: ReqRegisterBook) {
    const { items } = await this.bookRepository.fetchSimilarBooks(book.title)
    const mostSimilarBook = items ? items[0] : null
    const authorList = mostSimilarBook?.volumeInfo?.authors?.map(name => ({ name })) || [book.author]

    const registeredBook = await this.prismaService.book.create({
      data: {
        title: book.title,
        coverImageUrl: book.coverImageUrl || mostSimilarBook?.volumeInfo?.imageLinks?.thumbnail || null,
        description: book.description || mostSimilarBook?.volumeInfo.description || null,
        publishedAt: book.publishedAt || (mostSimilarBook === null ? null : new Date(mostSimilarBook!!.volumeInfo.publishedDate)),
        bookAuthors: {
          create: authorList.map((author) => {
            return {
              author: {
                connectOrCreate: {
                  where: {
                    name: author.name,
                  },
                  create: author,
                },
              },
            }
          }
          )
        }
      },
      ...BOOK_JOIN_POLICY
    })

    return registeredBook
  }

  searchBooks(order: ReqSearchOrderEnum, searchQuery?: string) {
    return this.prismaService.book.findMany({
      where: {
        title: {
          contains: searchQuery
        }
      },
      ...this.getOrder(order),
      ...BOOK_JOIN_POLICY
    })
  }

  private getOrder(order: ReqSearchOrderEnum): { orderBy: { registeredAt: "asc" | "desc" } | { lastReturnedAt: "asc" | "desc" } } {
    switch (order) {
      case ReqSearchOrderEnum.LATEST_RETURNED:
        return {
          orderBy: {
            lastReturnedAt:  "desc"
          },
        }
      case ReqSearchOrderEnum.LATEST_REGISTERED:
        return {
          orderBy: {
            registeredAt: "desc"
          },
        }
    }
  }
}
