import { Test } from '@nestjs/testing'
import { PrismaModule } from 'src/prisma/prisma.module'
import { BookController } from './book.controller'
import { BookRepository } from './book.repository'
import { BookService } from './book.service'

describe('CatsController', () => {
  let bookService: BookService
  // let bookController: BookController

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [PrismaModule],
      providers: [BookService, BookRepository],
      controllers: [BookController],
    }).compile()

    bookService = moduleRef.get<BookService>(BookService)
  })

  describe('登録', () => {
    it('正しく登録できるか', async () => {
      const res = await bookService.register({
        title: 'ビッグデータを支える技術',
        email: 'xxxxxxxxxx@morifuji-is.ninja',
        author: {
          name: '西田圭介',
        },
      })
    })

    it('存在しないタイトルでも正しく登録できるか', async () => {
      const res = await bookService.register({
        title: 'fwebukgewuhg42g2bvhi42ogh4o3qihg3ohg3qhi43giq',
        description: '##',
        publishedAt: new Date('2021-10-10'),
        email: 'xxxxxxxxxx@morifuji-is.ninja',
        coverImageUrl: '',
        author: {
          name: '##',
        },
      })
    })
  })
})
