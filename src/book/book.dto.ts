import {
  IsDate,
  IsDateString,
  IsDefined,
  IsEmail, IsNotEmpty, IsObject, IsOptional, MaxLength, Validate, ValidateNested,
} from 'class-validator'
// FIXME: typeはprovideできないのが不便。結局prismaに依存してしまっているので直したい...
// import { Author as AuthorType, Book as BookType } from '.prisma/client'
import { Transform, Type } from 'class-transformer'
import { ApiProperty } from '@nestjs/swagger'
import { iso8601Str2Date } from 'src/_transformer/iso8601Str2Date'

// export default class ReqRegisterAuthor implements Pick<AuthorType, 'name'> {
export class ReqRegisterAuthor {
  @IsNotEmpty()
  @IsDefined()
  @MaxLength(255, { message: '著者名は$constraint1文字以内で入力してください' })
  name: string
}

export enum ReqSearchOrderEnum {
  LATEST_REGISTERED = 'LATEST_REGISTERED',
  LATEST_RETURNED = 'LATEST_RETURNED',
}

export class ReqSearchBook {
  @IsOptional()
  @MaxLength(255, { message: '検索文字列は$constraint1文字以内で入力してください' })
  query?: string

  @IsOptional()
  @ApiProperty({ enum: ReqSearchOrderEnum, enumName: 'ReqSearchOrderEnum' })
  order?: ReqSearchOrderEnum
}

// nullableからundefinableにしたいができないのでimplしない
// export class ReqRegisterBook implements Pick<BookType, 'title' | 'coverImageUrl' | 'description' | 'publishedAt'> {
export class ReqRegisterBook {
  @MaxLength(255, { message: '説明文は$constraint1文字以内で入力してください' })
  @IsOptional()
  description?: string

  @IsDate({ message: '発売日時のフォーマットが正しくありません' })
  @IsOptional()
  @Transform((v) => iso8601Str2Date(v.value))
  publishedAt?: Date

  @IsNotEmpty()
  @MaxLength(255, { message: 'タイトルは$constraint1文字以内で入力してください' })
  title: string

  @MaxLength(255, { message: 'カバー画像URLは$constraint1文字以内で入力してください' })
  @IsOptional()
  coverImageUrl?: string

  @IsNotEmpty()
  @MaxLength(255, { message: 'カバー画像URLは$constraint1文字以内で入力してください' })
  @IsEmail({}, { message: 'メールアドレスのフォーマットが正しくありません' })
  email: string

  @IsNotEmpty({ message: "著者情報を入力してください" })
  @ValidateNested() // NOTE: arrayとobjectを区別しないので、IsObjectとIsArrayを必ず併用
  @IsObject({ message: "著者情報は不正なフォーマットです" })
  @Type(() => ReqRegisterAuthor) // NOTE: https://github.com/typestack/class-validator/issues/83#issuecomment-343434576
  author: ReqRegisterAuthor
}

class ResAuthor {
  @ApiProperty()
  id: string
  @ApiProperty()
  name: string
}

class ResBookAuthor {
  @ApiProperty()
  author: ResAuthor
}

export class ResBook {
  @ApiProperty()
  id: string
  @ApiProperty()
  title: string
  @ApiProperty()
  coverImageUrl: string | null
  @ApiProperty()
  description: string | null
  @ApiProperty()
  publishedAt: Date | null

  @ApiProperty()
  bookAuthors: ResBookAuthor[]
}