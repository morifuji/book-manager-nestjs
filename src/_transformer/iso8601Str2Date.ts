export const iso8601Str2Date = (raw: string) => {
    if (!raw) return undefined
    return new Date(raw)
}