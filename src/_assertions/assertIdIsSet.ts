
type AssertIdIsSet = (value: {id: string|number|undefined}) => asserts value is {id: string|number}
const assertIdIsSet: AssertIdIsSet = (value: {id: string|number|undefined}): asserts value is {id: string|number} => {
    if (value.id === undefined) {
        console.error("ID属性がundefinedになっています")
        throw new Error("ID属性がundefinedになっています")
    }
}