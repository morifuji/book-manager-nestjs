import { Module } from '@nestjs/common'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { UserModule } from './user/user.module'
import { BookModule } from './book/book.module'
import { BorrowingModule } from './borrowing/borrowing.module';

@Module({
  imports: [UserModule, BookModule, BorrowingModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
