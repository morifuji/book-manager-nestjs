import { Module } from '@nestjs/common';
import { PrismaModule } from 'src/prisma/prisma.module';

@Module({
    imports: [PrismaModule],
    exports: [PrismaModule]
})
export class ApplicationServiceTestModule {}
