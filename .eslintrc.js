module.exports = {
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: 'tsconfig.json',
    sourceType: 'module',
  },
  plugins: ['@typescript-eslint/eslint-plugin', "jest"],
  extends: [
    "airbnb-base",
    'plugin:@typescript-eslint/recommended',
  ],
  root: true,
  env: {
    node: true,
    es2021: true,
    jest: true
  },
  ignorePatterns: ['.eslintrc.js'],
  "settings": {
    "import/resolver": {
      "typescript": {}
    }
  },
  rules: {
    '@typescript-eslint/interface-name-prefix': 'off',
    '@typescript-eslint/explicit-function-return-type': 'off',
    '@typescript-eslint/explicit-module-boundary-types': 'off',
    '@typescript-eslint/no-explicit-any': 'off',
    "no-useless-constructor": "off",
    "import/no-extraneous-dependencies": "off",
    "semi": [
      "error",
      "never",
      {
        "beforeStatementContinuationChars": "never"
      }
    ],
    "semi-spacing": [
      "error",
      {
        "after": true,
        "before": false
      }
    ],
    "semi-style": [
      "error",
      "first"
    ],
    "no-extra-semi": "error",
    "no-unexpected-multiline": "error",
    "no-unreachable": "error",
    "import/extensions": "off",
    "class-methods-use-this": "off",
    "max-len": "off",
    "import/prefer-default-export": "off",
    "max-classes-per-file": "off",
    "@typescript-eslint/no-unused-vars": ["warning"],
    "no-console": ["error", { "allow": ["warn", "error"] }],
    "no-shadow": "off",
    "@typescript-eslint/no-shadow": "error",
    "@typescript-eslint/no-misused-promises": "error",
    "@typescript-eslint/no-floating-promises": "error"
  },
};
